﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._001_Creational
{
    /// <summary>
    /// Name : Abstract Factory . 
    /// Problem : When a family of products needs to be created and client can work only with abstractions
    /// Solution :
    ///     1. Abstract family of products 
    ///     2. Concrete family of products 
    ///     3. Anstract Product Generator 
    ///     4. Concrete product generators. 
    /// </summary>
    class _004_AbstractFactory
    {
        static void Main1(string[] args)
        {
            AbstractFactory factory = new ProductGenerator1();
            Console.WriteLine(factory.GetProductA().GetName());
            Console.WriteLine(factory.GetProductB().GetNumber());
            factory = new ProductGenerator2();
            Console.WriteLine(factory.GetProductA().GetName());
            Console.WriteLine(factory.GetProductB().GetNumber());
            Console.ReadLine();
        }
        #region Abstract Products
        public abstract class AbstractProductA
        {
            public abstract string GetName();
        }
        public abstract class AbstractProductB
        {
            public abstract string GetNumber();
        }
        #endregion
        #region Concrete Products
        public class ProductA1 : AbstractProductA
        {
            public override string GetName()
            {
                return "ProductA1";
            }
        }
        public class ProductA2 : AbstractProductA
        {
            public override string GetName()
            {
                return "ProductA2";
            }
        }
        public class ProductB1 : AbstractProductB
        {
            public override string GetNumber()
            {
                return "Product Number 1 ";
            }
        }
        public class ProductB2 : AbstractProductB
        {
            public override string GetNumber()
            {
                return "Product Number 2 ";
            }
        }

        #endregion
        #region Abstract Factory
        public abstract class AbstractFactory
        {
            public abstract AbstractProductA GetProductA();
            public abstract AbstractProductB GetProductB();
        }

        #endregion
        #region Abstract factory implementation
        public class ProductGenerator1 : AbstractFactory
        {
            public override AbstractProductA GetProductA()
            {
                return new ProductA1();
            }

            public override AbstractProductB GetProductB()
            {
                return new ProductB1();
            }
        }
        public class ProductGenerator2 : AbstractFactory
        {
            public override AbstractProductA GetProductA()
            {
                return new ProductA2();
            }

            public override AbstractProductB GetProductB()
            {
                return new ProductB2();
            }
        }

        #endregion
    }
}
