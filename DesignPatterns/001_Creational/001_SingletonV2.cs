﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._001_Creational
{
    class _001_SingletonV2
    {
        //Private static singleton instance 
        private static readonly Lazy<_001_SingletonV2> lazyInitializer = new Lazy<_001_SingletonV2>(() => new _001_SingletonV2());
        //public static singleton access method
        public static _001_SingletonV2 GetInstance { get { return lazyInitializer.Value; } }
        // default constructor
        private _001_SingletonV2()
        {

        }
        static void Main1(string[] args)
        {
            var i1 = _001_SingletonV2.GetInstance;
            var i2 = _001_SingletonV2.GetInstance;
            Console.WriteLine(i1 ==i2);
            Console.Read();
        }
    }
}
