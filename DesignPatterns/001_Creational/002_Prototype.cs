﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._001_Creational
{
    /// <summary>
    /// Name : Prototype 
    /// Problem : When we need to create a copy of an instance and use it in the application 
    /// solution : 1. Abstract class with clone method. 
    ///            2. Implementation with clone implementation returning the abstract class. 
    /// </summary>
    class _002_Prototype
    {

        static void Main1(string[] args)
        {
            Prototype t1 = new Type1();
            Prototype t2 = new Type2();
            Console.WriteLine(t1.Clone().GetId());
            Console.WriteLine(t2.Clone().GetId());
            Console.Read();
        }

    }
    abstract class Prototype
    {
        public abstract Prototype Clone();
        public abstract int GetId();
    }
    class Type1 : Prototype
    {

        public override Prototype Clone()
        {
            return (Prototype)this.MemberwiseClone();
        }

        public override int GetId()
        {
            return 10;
        }
    }
    class Type2 : Prototype
    {

        public override Prototype Clone()
        {
            return (Prototype)this.MemberwiseClone();
        }

        public override int GetId()
        {
            return 20;
        }
    }
}
