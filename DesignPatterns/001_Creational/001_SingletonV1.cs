﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._001_Creational
{
    /// <summary>
    /// 1. Pattern Name : Singleton
    /// 2. Problem : When we want the application to use only one instance 
    /// of a class in the system
    /// 3.Solution 
    ///     1. There should be a private parameterless constructor . 
    ///     2. A private static vairable which is the reference to the singleton instance. 
    ///     3. A public static method to get the instance. 
    /// </summary>
    class _001_SingletonV1
    {
        private static readonly Object _lock = new Object();
        //Private static singleton instance 
        private  static  _001_SingletonV1 SingletonInstance{ get; set; }
        //parameterless constructor
        public _001_SingletonV1()
        {

        }
        //public static method to retrieve singleton instance 
        //Using double locking . 
        public static _001_SingletonV1 GetInstance()
        {
            if (SingletonInstance == null) {
                lock (_lock) {
                    if (SingletonInstance == null) {
                        SingletonInstance = new _001_SingletonV1();
                    }
                }
            }
            return SingletonInstance;
        }
        static void Main1(string[] args)
        {
            var i1 = _001_SingletonV1.GetInstance();
            var i2 = _001_SingletonV1.GetInstance();
            Console.WriteLine(i1 == i2 );
            Console.ReadLine();
        }
    }
}
