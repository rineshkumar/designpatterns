﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._001_Creational
{
    /// <summary>
    /// Name 
    /// Name : Factory Method 
    /// Problem : When instance creation needs to be delegated to implementations . 
    /// Solution : 
    ///    1. Interface with the factory class declared 
    ///    2. Implementation with the implementation for above interface 
    ///    3.Product interface 
    /// </summary>
    class _003_FactoryMethod
    {
        static void Main1(string[] args)
        {
            IProductGenerator pg1 = new ProductGenerator1();
            IProductGenerator pg2 = new ProductGenerator2();
            Console.WriteLine(pg1.GetProduct().GetName());
            Console.WriteLine(pg2.GetProduct().GetName());
            Console.Read();
        }
    }
    interface IProduct
    {
        string GetName();
    }
    class Product1 : IProduct
    {

        public string GetName()
        {
            return "Product1";
        }
    }
    class Product2 : IProduct
    {

        public string GetName()
        {
            return "Product2";
        }
    }
    interface IProductGenerator
    {
        IProduct GetProduct();
    }
    class ProductGenerator1 : IProductGenerator
    {

        public IProduct GetProduct()
        {
            return new Product1();
        }
    }
    class ProductGenerator2 : IProductGenerator
    {

        public IProduct GetProduct()
        {
            return new Product2();
        }
    }
}
