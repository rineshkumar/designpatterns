﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._001_Creational
{
    /// <summary>
    /// Name : Builder 
    /// Problem :  Separating the construction process of an instance from the instance itself
    /// Solution : 
    ///         1. Builder interface with contract for construction process. 
    ///         2. Builder implementations with various implemntations for construction process
    ///         returning the product 
    /// </summary>
    class _005_Builder
    {
        static void Main1(string[] args)
        {
            ProductBuilder p1 = new Builder1();
            Product p = p1.BuildProduct();
            p.ShowParts();
            ProductBuilder p2 = new Builder2();
            p = p2.BuildProduct();
            p.ShowParts();
            Console.ReadLine();
        }
    }
    #region Final Product
    public class Product
    {
        public List<string> parts;
        
        public Product()
        {
            parts = new List<string>();
        }
        public void ShowParts()
        {
            foreach (var item in parts)
            {
                Console.WriteLine(item);
            }
        }
    }
    #endregion
    #region The product builder contract
    interface ProductBuilder
    {
        Product BuildProduct();
    }
    #endregion
    #region Product builder implementations
    public class Builder1 : ProductBuilder
    {
        Product p = new Product();
        public Product BuildProduct()
        {
            p.parts.Add("Part11");
            p.parts.Add("Part12");
            p.parts.Add("Part13");
            return p;
        }
    }
    public class Builder2 : ProductBuilder
    {
        Product p = new Product();
        public Product BuildProduct()
        {
            p.parts.Add("Part21");
            p.parts.Add("Part22");
            p.parts.Add("Part23");
            return p;
        }
    }
    #endregion

}
