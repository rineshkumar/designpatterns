﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._002_Structural
{
    /// <summary>
    /// Name : Bridge
    /// Problem : When we want abstraction used in client to change and in addition to that 
    /// some dependent implementation also 
    /// Solution : 
    /// 1. Have multiple implementation ready for implementor 
    /// 2. Provide implementor to the abstraction.
    /// </summary>
    class _003_BridgePattern
    {
        static void Main1(string[] args)
        {
            IClientAbstraction ca = new ClientImplementation1(new Implementor1());
            Console.WriteLine(ca.GetName());
            ca = new ClientImplementation1(new Implementor2());
            Console.WriteLine(ca.GetName());
            Console.Read();
        }
        
    }
    #region Client abstraction
    public interface IClientAbstraction
    {
        string GetName();
    }
    public class ClientImplementation1 : IClientAbstraction
    {
        IImplementor implementor;
        public ClientImplementation1(IImplementor implementor)
        {
            this.implementor = implementor;
        }
        IImplementor implemetor;
        public string GetName()
        {
            return implementor.GetImplementorName();
        }
    }
    #endregion
    #region Implementor
    public interface IImplementor
    {
        string GetImplementorName();
    }
    public class Implementor1 : IImplementor
    {

        public string GetImplementorName()
        {
            return "Implementor 1 ";
        }
    }
    public class Implementor2 : IImplementor
    {

        public string GetImplementorName()
        {
            return "Implementor 2 ";
        }
    }
    #endregion
}
