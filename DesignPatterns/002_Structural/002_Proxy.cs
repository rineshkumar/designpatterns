﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._002_Structural
{
    /// <summary>
    /// Name : Proxy 
    /// Problem : When creation of original implementation is expensive . 
    /// SOlution : Client will use proxy class which will instantiate main class only on need basis 
    /// </summary>
    class _002_Proxy
    {
        static void Main1(string[] args)
        {
            IMainInterface m = new ProxyClass();
            Console.WriteLine(m.GetName());
            Console.ReadLine();
        }
    }
    #region Main interface and implementation
    public interface IMainInterface
    {
        string GetName();
    }

    public class MainImplementation :IMainInterface
    {

        public string GetName()
        {
            return "Main Name ";
        }
    }
    #endregion
    #region Proxy Implementation
    public class ProxyClass :IMainInterface
    {
        IMainInterface implementation;
        public string GetName()
        {
            if (implementation == null)
            {
                implementation = new MainImplementation();
            }
            return implementation.GetName();
        }
    }
    #endregion
}
