﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._002_Structural
{
    /// <summary>
    /// Name : Decorator 
    /// Problem : We want to add additional responsity to the implementation without changing the implementation . 
    /// Solution : 1. Create Main interface 
    ///             2. Create main implementation . 
    ///             3.Create decorator implementation using main implementation . 
    /// </summary>
    class _005_Decorator
    {
        static void Main1(string[] args)
        {
            IMain mainImplementation = new MainImpl();
            IMain decorator = new Decorator(mainImplementation);
            Console.WriteLine(decorator.GetName());
            Console.Read();
        }
    }
    #region Main Interface and implementation
    public interface IMain
    {
        string GetName();
    }
    public class MainImpl : IMain
    {
        public string GetName()
        {
            return "Hello world";
        }
    }
    #endregion
    #region Decorator implementation 
    public class Decorator : IMain 
    {
        private IMain implementation;
        public Decorator(IMain mainImplementation)
        {
            this.implementation = mainImplementation;
        }
        public string GetName()
        {
            return "Decorated " + implementation.GetName();
        }
    }
    #endregion
}
