﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._002_Structural
{
    class _007_FlyWeight
    {
        static void Main1(string[] args)
        {
            for (int i = 0; i < 10; i++)
            {
                FlyWeightFactory.GetFlyWeightObject(i).ShowDetails();
            }
            Console.ReadLine();
        }
    }
    #region FlyWeightObjectTypes, contract and factory 
    public enum VehicleType
    {
        TwoWheeler,
        FourWheeler
    }
    public interface IVehicle
    {
        void ShowDetails();
    }
    public class FlyWeightFactory
    {
        static Dictionary<VehicleType, IVehicle> Vehicles = new Dictionary<VehicleType, IVehicle>();
        public static IVehicle GetFlyWeightObject(int i ){
            IVehicle vehicle;
            if (i % 2 == 0) {

                if (!Vehicles.TryGetValue(VehicleType.FourWheeler,out vehicle))
                {
                    Vehicles.Add(VehicleType.FourWheeler,new Car());
                }
                return Vehicles[VehicleType.FourWheeler];
            }
            else
            {
                if (!Vehicles.TryGetValue(VehicleType.TwoWheeler, out vehicle))
                {
                    Vehicles.Add(VehicleType.TwoWheeler, new bike());
                }
                return Vehicles[VehicleType.TwoWheeler];
            }
        }
    }
    #endregion
    #region FLyWeight concrete implementation
    public class Car : IVehicle
    {
        public void ShowDetails()
        {
            Console.WriteLine("This is a four wheeler ");
        }
    }
    public class bike : IVehicle
    {
        public void ShowDetails()
        {
            Console.WriteLine("This is a two wheeler");
        }
    }
    #endregion



}
