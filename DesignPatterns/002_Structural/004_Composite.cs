﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._002_Structural
{
    /// <summary>
    /// Name : Composite 
    /// Problem : When we have to compose object in to tree like structure 
    /// where the component and subcomponents are of same type. 
    /// Solution : Create component abstraction . 
    /// Create an implementation for data and one for childElements
    /// </summary>
    class _004_Composite
    {
        static void Main1(string[] args)
        {
            Component root = new Composite("root");

            root.Add(new Composite("A"));
            root.Add(new Composite("B"));
            root.Add(new Composite("c"));
            root.Add(new Leaf("Leaf "));
            root.Display();
            Console.ReadLine();

        }
    }
    #region Component
    public abstract class Component
    {
        public string Name { get; set; }
        public Component(string name)
        {
            this.Name = name;
        }
        public abstract void Add(Component c);
        public abstract void Display();
    }
    #endregion
    #region Composite and Leaf implementations
    public class Composite : Component
    {
        public Composite(string name)
            : base(name)
        {

        }
        private List<Component> childNodes = new List<Component>();
        public override void Add(Component c)
        {
            childNodes.Add(c);
        }

        public override void Display()
        {
            Console.WriteLine(Name);
            foreach (var item in childNodes)
            {
                item.Display();
            }
        }
    }


    public class Leaf : Component
    {
        public Leaf(string name)
            : base(name)
        {

        }
        public override void Add(Component c)
        {
            Console.WriteLine("Cannot add to leaf");
        }

        public override void Display()
        {
            Console.WriteLine(Name);
        }
    }


    #endregion
}
