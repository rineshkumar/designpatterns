﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._002_Structural
{
    /// <summary>
    /// Name : Facade
    /// Problem : When we wany to call many sub system components 
    /// Solution : Have a facade wrapper 
    /// </summary>
    class _006_Facade
    {
        
        static void Main1(string[] args)
        {
            IFacade f = new FacadeClass();
            f.SayMessage1();
            f.SayMessage2();
            Console.ReadLine();
        }
    }
    #region Subsystem classes
    public class HelloClass
    {
        public void SendMessage() {
            Console.WriteLine("Hello ");
        }
    }
    public class WorldClass
    {
        public void sendMessage() {
            Console.WriteLine("World");
        }
    }
    #endregion
    #region Facade interface and immplementation 
    public interface IFacade
    {
        void SayMessage1();
        void SayMessage2();
    }
    public class FacadeClass : IFacade
    {
        public void SayMessage1()
        {
            HelloClass h = new HelloClass();
            WorldClass w = new WorldClass();
            h.SendMessage();
            w.sendMessage();
            
        }

        public void SayMessage2()
        {
            HelloClass h = new HelloClass();
            WorldClass w = new WorldClass();
            w.sendMessage();
            h.SendMessage();
        }
    }

    
    #endregion
}
