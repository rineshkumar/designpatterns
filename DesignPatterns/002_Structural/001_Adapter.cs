﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._002_Structural
{
    /// <summary>
    /// Name : Adapter 
    /// Problem : When we need to use an interface incompatible with the system. 
    /// Solution : In new implementation keep old implementation and use it when called by client.
    /// </summary>
    class _001_Adapter
    {
        static void Main1(string[] args)
        {
            IOldInterface oldImpl = new OldClass();
            IAdapterInterface i = new NewAdapterImplentation(oldImpl);
            Console.WriteLine(i.GetName());
            Console.ReadLine();
        }
    }
    #region Old interface and implementation .
    public interface IOldInterface
    {
        string GetName();
    }
    public class OldClass : IOldInterface
    {
        public string GetName()
        {
            return "OldName";
        }
    }
    #endregion
    #region Adapter Interface and implementation
    public interface IAdapterInterface
    {
        string GetName();
    }
    public class NewAdapterImplentation : IAdapterInterface
    {
        public IOldInterface OldImplementation { get; set; }
        public string GetName()
        {
            return OldImplementation.GetName();
        }
        public NewAdapterImplentation(IOldInterface OldImplementation)
        {
            this.OldImplementation = OldImplementation;
        }
    }

    #endregion
}
