﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._003_Behavioral
{
    /// <summary>
    /// Name : Chain of responsiblity. 
    /// Problem : Providing more than one handler to handle a request . 
    /// Solution : 
    /// 1.Have an abstract class with abstract method handle request and concrete method setnext handler 
    /// 2. Create implementations using this abstract class.
    /// 3. Chain the classes using the SetNextHandler
    /// </summary>
    class _001_ChainOfResponsibility
    {
        static void Main1(string[] args)
        {
            RequestHandler handler1 = new RequestHandler1();
            RequestHandler handler2 = new RequestHandler2();
            RequestHandler handler3 = new RequestHandler3();

            handler1.SetNextRequestHandler(handler2);
            handler2.SetNextRequestHandler(handler3);

            for (int i = 0; i < 15; i++)
            {
                handler1.HandleRequest(i);
            }
            Console.ReadLine();
        }
    }
    #region Handler Contract and concrete handler
    public abstract class RequestHandler
    {
        protected RequestHandler _nextRequestHandler;
        public abstract void HandleRequest(int request);
        public void SetNextRequestHandler(RequestHandler nextHandler)
        {
            this._nextRequestHandler = nextHandler;
        }
    }
    public class RequestHandler1 : RequestHandler
    {

        public override void HandleRequest(int request)
        {
            if (request <= 5)
            {
                Console.WriteLine("Request Handler 1 is handling request " + request);
            }
            else
            {
                _nextRequestHandler.HandleRequest(request);
            }
        }
    }
    public class RequestHandler2 : RequestHandler
    {

        public override void HandleRequest(int request)
        {
            if (request <= 10)
            {
                Console.WriteLine("Request Handler 2 is handling request " + request);
            }
            else
            {
                _nextRequestHandler.HandleRequest(request);
            }
        }
    }
    public class RequestHandler3 : RequestHandler
    {

        public override void HandleRequest(int request)
        {
            if (request <= 15)
            {
                Console.WriteLine("Request Handler 3 is handling request "+request);
            }
            
        }
    }
    #endregion
}
