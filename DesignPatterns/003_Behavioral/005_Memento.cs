﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._003_Behavioral
{
    /// <summary>
    /// Name : Memento 
    /// Scenario : When we want to backup and restore the state of an object 
    /// 
    /// </summary>
    class _005_Memento
    {
        static void Main1(string[] args)
        {
            Originator o = new Originator(1, "a");
            o.ShowDetails();
            Memento m = o.CreateSnapshot();
            o.State1 = 2;
            o.ShowDetails();
            o.updateFromSnapShot(m);
            o.ShowDetails();
            Console.Read();
        }
    }
    #region Originator, Memento and caretaker classes

    public class Originator
    {
        public int State1 { get; set; }
        public string State2 { get; set; }
        public Originator(int state1, string state2)
        {
            this.State1 = state1;
            this.State2 = state2;
        }
        public Memento CreateSnapshot()
        {
            return new Memento(State1, State2);
        }
        public void updateFromSnapShot(Memento m)
        {
            this.State1 = m.State1;
            this.State2 = m.State2;
        }
        public void ShowDetails()
        {
            Console.WriteLine("State 1 = "+State1+" State2 = "+State2);
        }

    }
    public class Memento
    {
        public int State1 { get; set; }
        public string State2 { get; set; }
        public Memento(int state1, string state2)
        {
            this.State1 = state1;
            this.State2 = state2;
        }
    }
    
    #endregion
}
