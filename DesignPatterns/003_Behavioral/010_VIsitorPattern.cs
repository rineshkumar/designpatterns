﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._003_Behavioral
{
    /// <summary>
    /// Name : Visitor Pattern 
    /// Scenario : When we want to perform operation on a colleciton of items of different types.
    /// solution :
    ///     All the types should implmement IVisitable accept method. 
    ///     Visitor interface should support visit method for all the types. 
    /// </summary>
    class _010_VIsitorPattern
    {
        static void Main(string[] args)
        {
            List<IVisitable> objectList = new List<IVisitable>();
            objectList.Add(new OT1());
            objectList.Add(new OT2());
            IVisitor visitor = new Visitor1();

            foreach (IVisitable item in objectList)
            {
                item.Accept(visitor);
            }
            visitor = new Visitor2();
            foreach (IVisitable item in objectList)
            {
                item.Accept(visitor);
            }
            Console.ReadLine();
        }
    }
    #region IVisitable and IVisitor contract
    public interface IVisitable
    {
        void Accept(IVisitor visitor);
    }
    public interface IVisitor
    {
        void Visit(OT1 objectType);
        void Visit(OT2 objectType);
    }
    #endregion
    #region Differnt object types
    public class OT1 : IVisitable
    {
        public void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
    public class OT2 : IVisitable
    {
        public void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class Visitor1 : IVisitor
    {
        public void Visit(OT1 objectType)
        {
            Console.WriteLine(objectType.GetType().Name + " Visited by "+this.GetType().Name);
        }

        public void Visit(OT2 objectType)
        {
            Console.WriteLine(objectType.GetType().Name + " Visited by " + this.GetType().Name);
        }
    }
    public class Visitor2 : IVisitor
    {
        public void Visit(OT1 objectType)
        {
            Console.WriteLine(objectType.GetType().Name + " Visited by " + this.GetType().Name);
        }

        public void Visit(OT2 objectType)
        {
            Console.WriteLine(objectType.GetType().Name + " Visited by " + this.GetType().Name);
        }
    }

    #endregion
}
