﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._003_Behavioral
{
    /// <summary>
    /// Name : Observer 
    /// Scenario : When updates on a subject needs to be propagated to N observers 
    /// </summary>
    class _006_Observer
    {
        
        static void Main1(string[] args)
        {
            ISubject subject = new Subject();
            subject.AddObserver(new Observer1());
            subject.AddObserver(new Observer2());
            subject.setStateInfo("Hello ");
            subject.NotifyObservers();
            Console.ReadLine();
        }
    }
    #region Subject and Observer contract
    public interface ISubject
    {
        void AddObserver(IObserver observer);
        void NotifyObservers();
        string getStateInfo();
        void setStateInfo(string state);
    }
    public interface IObserver
    {
        void notify(ISubject subject);
    }

    #endregion
    #region Observer and Subject implementation
    public class Subject : ISubject
    {
        public List<IObserver> ObserverList { get; set; }
        public String state { get; set; }
        
        public void AddObserver(IObserver observer)
        {
            if (ObserverList == null)
                ObserverList = new List<IObserver>();
            ObserverList.Add(observer);
        }

        public void NotifyObservers()
        {
            foreach (var observer in ObserverList)
            {
                observer.notify(this);
            }
        }


        public string getStateInfo()
        {
            return state;
        }


        public void setStateInfo(string state)
        {
            this.state = state;
        }
    }
    public class Observer1 : IObserver
    {
        public void notify(ISubject subject)
        {
            Console.WriteLine("Received info 1 "+subject.getStateInfo());
        }
    }
    public class Observer2 : IObserver
    {
        public void notify(ISubject subject)
        {
            Console.WriteLine("Received info 2 " + subject.getStateInfo());
        }
    }
  
    #endregion
}
