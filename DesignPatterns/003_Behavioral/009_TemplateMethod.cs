﻿using System;

namespace DesignPatterns._003_Behavioral
{
    /// <summary>
    /// Name : Template Method . 
    /// Scenario : In case we have to follow a standard set of steps with some variation .
    /// Solution : Create abstract class with a method which defines the process. 
    /// The method which will change on a case to case needs to be abstract 
    /// </summary>
    class _009_TemplateMethod
    {
        static void Main1(string[] args)
        {
            Process p = new Process1();
            p.FollowProcess();
            p = new Process2();
            p.FollowProcess();
            Console.ReadLine();
        }
    }
    public abstract class Process
    {
        public void FollowProcess()
        {
            DoGenericActivity1();
            DoGenericActivity2();
            DoSpecificActivity();
        }

        public  abstract void DoSpecificActivity();

        private void DoGenericActivity2()
        {
            Console.WriteLine("Doing generic activity 1 ");
        }

        private void DoGenericActivity1()
        {
            Console.WriteLine("Doing generic activity 1 ");
        }
    }
    public class Process1 : Process
    {

        public override void DoSpecificActivity()
        {
            Console.WriteLine("Doing specific activity in process 1");
        }
    }
    public class Process2 : Process
    {
        public override void DoSpecificActivity()
        {
            Console.WriteLine("Doing specific activity in process 2");
        }
    }

}
