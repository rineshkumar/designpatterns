﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._003_Behavioral
{
    class _003_Iterator
    {
        static void Main1(string[] args)
        {
            // Build a collection
            MyCollection collection = new MyCollection();
            collection[0] = new Item("Item 0");
            collection[1] = new Item("Item 1");
            collection[2] = new Item("Item 2");
            collection[3] = new Item("Item 3");
            collection[4] = new Item("Item 4");
            collection[5] = new Item("Item 5");
            collection[6] = new Item("Item 6");
            collection[7] = new Item("Item 7");
            collection[8] = new Item("Item 8");

            // Create iterator
            MyIterator iterator = collection.GetIterator();

            // Skip every other item
            iterator.Step = 2;

            Console.WriteLine("Iterating over collection:");

            for (Item item = iterator.First();
                !iterator.IsDone; item = iterator.Next())
            {
                Console.WriteLine(item.Name);
            }

            // Wait for user
            Console.ReadKey();
        }
    }
    #region Item
    
    #endregion
    #region Collection and iterator contract

    public interface IIterator
    {
        Item First();
        Item Next();
        bool IsDone { get; }
        Item CurrentItem { get; }

    }
    public interface ICollection
    {
        MyIterator GetIterator();
    }

    #endregion
    #region Item , Collection and Iterator implementation
    public class Item
    {
        private string _name;
        public Item(string item)
        {
            _name = item;
        }
        public string Name
        {
            get
            {
                return _name;
            }
        }
    }
    public class MyCollection : ICollection
    {
        private ArrayList _items = new ArrayList();
        public MyIterator GetIterator()
        {
            return new MyIterator(this);
        }
        public object this[int index]
        {
            get { return _items[index]; }
            set { _items.Add(value); }
        }
        public int Count
        {
            get { return _items.Count; }
        }


       
    }
    public class MyIterator : IIterator
    {
        private MyCollection _collection;
        private int _current = 0;
        private int _step = 1;
        public MyIterator(MyCollection collection)
        {
            this._collection = collection;
        }
        public Item First()
        {
            _current = 0;
            return _collection[_current] as Item;
        }

        public Item Next()
        {
            _current += _step;
            if (!IsDone)
                return _collection[_current] as Item;
            else
                return null;
        }

        public bool IsDone
        {
            get { return _current >= _collection.Count; }
        }

        public Item CurrentItem
        {
            get { return _collection[_current] as Item; }
        }
        public int Step
        {
            get { return _step; }
            set { _step = value; }
        }
    }

     

    #endregion

}
