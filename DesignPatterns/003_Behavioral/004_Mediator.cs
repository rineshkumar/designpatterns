﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._003_Behavioral
{
    /// <summary>
    /// Name : Mediator 
    /// Scenario :  When coordination between items needs to happen using a central control 
    /// Solution : Create Mediator (Central control ) with provision to add participants
    ///             Accept message from a participant and distribute among other participants 
    ///             Create participant with knowledge of the mediator , sending message to the mediator 
    ///             and receiving message from the mediator.
    /// 
    /// </summary>
    class _004_Mediator
    {
        static void Main1(string[] args)
        {
            IMediator m = new Mediator();
            List<IParticipant> participantList = new List<IParticipant>();
            for (int i = 0; i < 10; i++)
            {
                IParticipant p
                    = new Participant("Participant " + i, m);
                participantList.Add(p);
                m.AddParticipant(p);
            }
            participantList.First().PublishMessage("hello World ");
            Console.Read();
        }
    }
    #region Mediator and Participant contracts
    public interface IParticipant
    {
        void PublishMessage(string message);
        void ReceiveMesage(string message);
    }
    public interface IMediator
    {
        void AcceptMessage(string message);
        void DistributeMesssage(IParticipant p,  string message);
        void AddParticipant(IParticipant participant);
    }
    #endregion
    #region Participant and Mediator implementation
    public class Participant : IParticipant
    {
        public IMediator mediator { get; set; }
        public string name { get; set; }
        public Participant(string name, IMediator mediator)
        {
            this.mediator = mediator;
            this.name = name;
        }
        public void PublishMessage(string message)
        {
            mediator.AcceptMessage(message);
        }

        public void ReceiveMesage(string message)
        {
            Console.WriteLine(name + " processing message " + message);
        }
    }
    public class Mediator : IMediator
    {
        private List<IParticipant> _list = new List<IParticipant>();
     
        public void AcceptMessage(string message)
        {
            foreach (var item in _list)
            {
                DistributeMesssage(item,message);
            }
        }

        public void DistributeMesssage(IParticipant p,  string message)
        {
            p.ReceiveMesage(message);
        }


        public void AddParticipant(IParticipant participant)
        {
            _list.Add(participant);
        }
    }


    #endregion
}
