﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._003_Behavioral
{
    /// <summary>
    /// Name : State Pattern 
    /// Scenario : 
    /// When object needs to switch its state based on a condition . 
    /// State : The object which has all processing logic . 
    /// Solution : Implement the state interface which has all the processing logic . 
    ///            Context class should have capability to switch state . 
    ///            State implementations should have access to the context objects . 
    /// </summary>
    class _007_States
    {
        static void Main1(string[] args)
        {

            StateContext sc = new StateContext();
            sc.PrintState();
            sc.PrintState();
            sc.PrintState();
            sc.PrintState();
            Console.Read();
        }
    }
    #region Statecontract and implementation
    public interface IState
    {
        void WriteName(StateContext context, string name);
    }
    public class StateLower : IState
    {

        public void WriteName(StateContext context, string name)
        {
            Console.WriteLine(name.ToLower());
            context.SetState(new StateUpper());
        }
    }
    public class StateUpper : IState
    {
        public void WriteName(StateContext context, string name)
        {
            Console.WriteLine(name.ToUpper());
            context.SetState(new StateLower());
        }
    }

    #endregion
    #region State and context implementation

    public class StateContext
    {
        private IState State { get; set; }
        private string InstanceState { get; set; }
        public StateContext()
        {
            this.State = new StateLower();
            InstanceState = "Hello World "; 
        }
        public void PrintState()
        {
            State.WriteName(this, InstanceState);
          
        }
        public void SetState(IState state) {
            this.State = state;
        }
    }
    #endregion



}
