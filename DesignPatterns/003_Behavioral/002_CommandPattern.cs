﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._003_Behavioral
{
    /// <summary>
    /// Name : Command Pattern 
    /// Problem : When all the information required to perform an action 
    /// is encapsulated in an object with the method to be called and parameters required to call
    /// the method . 
    /// Command object encapsualtes the receiver object which performs the action . 
    /// Solution : 
    /// 1. Implement IReceiver interface with Action method
    /// 2.Set this receiver in the Command abstract class and provide implementation 
    /// for execute abstract method.
    /// 3.Create invoker class which will take Command and receiver as input and will set 
    /// receiver in command
    /// 4. Client will send command and receiver to the invoker . 
    /// </summary>
    class _002_CommandPattern
    {
        static void Main1(string[] args)
        {
            Command c = new ConcreteCommand(new Reciver());
            Invoker i = new Invoker(c);
            i.InvokeCommand();
            Console.ReadLine();
        }
    }
    #region Invoker, Command and Receiver declaration
    public interface IReceiver
    {
        void Action();
    }
    public abstract class Command
    {
        public IReceiver receiver { get; set; }

        public Command(IReceiver receiver)
        {
            this.receiver = receiver;
        }
        public abstract void Execute();
    }
    public class Invoker
    {
        public Command Command { get; set; }
        public Invoker(Command c)
        {
            this.Command = c;
        }
        public void InvokeCommand()
        {
            Command.Execute();
        }
    }

    #endregion
    #region Concrete implementations for command and receiver
    public class ConcreteCommand : Command
    {

        public ConcreteCommand(IReceiver receiver)
            : base(receiver)
        {

        }

        public override void Execute()
        {
            receiver.Action();
        }
    }
    public class Reciver : IReceiver
    {
        public void Action()
        {
            Console.WriteLine("Receiver 1 executing the command ");
        }
    }

    #endregion
}
