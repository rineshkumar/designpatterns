﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._003_Behavioral
{
    /// <summary>
    /// Name : Strategy Pattern 
    /// Scenario : When we want to switch the implementation in run time 
    /// Solution . : Create Algorithm interface and implementations. 
    ///             Switch algorithms during run time based on condition. 
    /// </summary>
    class _008_StrategyPattern
    {
        static void Main1(string[] args)
        {
            IAlgorithm algo;
            Random r = new Random();
            for (int i = 0; i < 20; i++)
            {
                
                int rInt = r.Next(1, 3); //for ints
                algo = GetAlgorithm(rInt);
                algo.ProcessData("Hello world");
            }
            Console.ReadLine();

        }

        private static IAlgorithm GetAlgorithm(int rInt)
        {
           
            switch (rInt)
            {
                case 1:
                    return new Algorithm1();

                case 2:
                    return new Algorithm2();

                case 3:
                    return new Algorithm3();

                default:
                    return new Algorithm3();
            }
        }
    }
    #region Algorithm Interface
    public interface IAlgorithm
    {
        void ProcessData(string name);
    }
    public class Algorithm1 : IAlgorithm
    {
        public void ProcessData(string name)
        {
            Console.WriteLine(name.ToUpper());
        }
    }
    public class Algorithm2 : IAlgorithm
    {
        public void ProcessData(string name)
        {
            Console.WriteLine(name.ToLower());
        }
    }
    public class Algorithm3 : IAlgorithm
    {
        public void ProcessData(string name)
        {
            Console.WriteLine(name.Reverse());
        }
    }

    #endregion
}
